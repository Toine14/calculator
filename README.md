# Calculator

Git training project.
c'est le premier projet géré avec Git
Auteur: moi


# Voici les principales commandes markdown

https://openclassrooms.com/fr/courses/1304236-redigez-en-markdown

# Titre


## Sous_titre


En **gras**

_italique_

* liste
* liste


# Commandes sous GIT
* git status : renvoi les modifications s'il y en a eu
	==> fichiers modifiés (des fois il renvoi fichiers dirty "means incohérences" )
	==> fichiers supprimés
	==> nouveaux fichiers


==> Avant de faire un commit, il faut configurer son compte
(on peut changer les options localement (remplacer global par local) pour appliquer ces options à un dossier en particulier)
Si la config est changée localement: les infos seront dans le fichier config du dossier .git du projet concerné
Si la config est changée globalement: les infos sont stockée dans le fichier c:\users\nom du user\ .gitconfig

* git config --global user.name 
* git config --global user.email 
* git config --get user.name
* git config --get user.email

* git add README.md
* git add --all

on dit tt ce qu'on veut commiter et ensuite on commit l'ensemble

* git commit -m "message de type du commit"

commit=commettre, signifie que l'on envoie le fichier sur le serveur (souvent on code, on test puis on commit).
	==> Attention commiter trop souvent va faire perdre du temps.
	==> Attention commiter pas assez souvent va rendre la taille des commits trop lourde.
* git add --all

* git push (par défaut vers le serveur origin)

==> En cas de mauvaise authentification, Menu démarrer windows> gestionnaire d'authentification> 


* git remote
* git remote -v
==> On pourrait avoir d'autres dépots que origin les uns à coté des autres

* git remote add serveur2 https://ici on rentre l'URL de notre projet

* git remote remove serveur2
* git remote rename serveur2 server2
* git push server2

==> Pour éviter de se retrouver avec des fichiers parasite de type .class on créé un fichier .gitignore
dans ce fichier 


suppression des fichiers au cas où: bien sur on pense à inclure ce fichier dans le .gitignore

* git rm --cached
* git 
==> on oublie pas de faire un commit et push après pour tout supprimer.

* git pull ==> aller chercher depuis cloud


--------------------------------------------
Lundi 18/03/2019
* git fetch ("means récupérer, aller chercher") ==> on va chercher à distance les versions de tous les dépots mais ne fusionne pas les modifications. On travaille sur des branches : un commit crée une branche (une branche en bout de branche ou si quelqu'un travaille sur le même code, une branche qui dérive)
* git merge ==> mélange: commande la plus complexe car elle va impliquer le plus de problème.
on se place sur la branche où l'on est et on merge ce qu'on veur ajouter.
* git merge FETCH_HEAD ==> vient merger ce que je viens de fetcher précédement: il vient ajouter les modifications qui viennent du dernier fetch.

* git branch: lister les branches
* git branch -v : liste des branches et leur dernier commit avec le texte et le numéro (7 caractère dans le numéro : c'est juste pour afficher une partie du nom)

* git branch -v - r :  montre moi les branches sur le remote. Permets de vérifier que l'on est bien à jour. permet de travailler sur certaines branches (exemple versions d'autres personnes)

Dans le répertoire .git>refs>heads>master (bout de la branche locale)
				   .git>remotes>origin>master (bout de la branche du master sur le serveur)

* git log : voir tous les commit qui ont été fait
* git branch nom : créé une branche de nom nom
* git checkout nom : on passe sur la branche nom. Copie collie de tous les fichiers

----------------------------------------------------
Par exemple: on veut aller chercher un projet sur git lab qui nous plait mais on veut le modifier légérement: on va le cloner et puis on créer une nouvelle branche.
-------------------------------------------------
si on créer une branche en locale, il faut aussi la créer sur le cloud:

* git push --set-upstream origin nom: on a créé cette branche sur le remote

* git branch -u origin/usa usa
* git merge master
---------------------------------------------------
* conflit : généralement au cours d'un merge ou d'un pull




* git branch -d nomdelabranche (si on remplace par un grand D on force la supression même si des modifications n'ont pas aplliquées à d'autres branches)


exemple on veut faire des modifications sur une branche puis les intégrer au master:
> git branch r10 (création branche)
> git checkout r10
> git add Main.java (ici fichier main)
> git commit -m "blabla"
> git checkout master
> git merge r10 (on peut ajouter l'option --no-commit r10 et faire un commit après avec le message que l'on veut)
> git push
> git branch -d r10 (supprimer branche r10)
-------------------------------------------------------------
## Historique

* git show 8ca54b50 (mettre un morceau du numéro de commit et il montre les modifications)
* git show HEAD (montre le dernier commit fait)
* git show "HEAD^" = HEAD^1 (montre le parent du commit)
* git show "HEAD^^" = HEAD^1^1 (montre le parent du parent)
* git show "HEAD^2"

* git help show ==> renvoie la doc 
* git diff ==> voir les différences entre maintenant et le dernier commit.
* git diff HEAD ==> renvoie les différences entre maintenant et ce qui a déjà subit des add
* git diff numéro de commit et HEAD ==> voir les différences entre le dernier commit et un commit donné
* git diff master usa ==> différence entre master et usa(en rouge master en vert usa)
* git diff master usa Main.java (spécifiquement sur le fichier Main.java)

